#include <OneWire.h>
#include <DallasTemperature.h>
#include <LiquidCrystal_PCF8574.h>
#include <Wire.h>
#include <TroykaDHT.h>

#define RELAY1 7
#define RELAY2 8

LiquidCrystal_PCF8574 lcd(0x27); // set the LCD address to 0x27 for a 16 chars and 2 line display


int show = -1;

DHT dht(4, DHT11);

// Data wire is plugged into port 2 on the Arduino
#define ONE_WIRE_BUS 2
#define TEMPERATURE_PRECISION 6 // Lower resolution

// Setup a oneWire instance to communicate with any OneWire devices (not just Maxim/Dallas temperature ICs)
OneWire oneWire(ONE_WIRE_BUS);


// Pass our oneWire reference to Dallas Temperature. 
DallasTemperature sensors(&oneWire);

int numberOfDevices; // Number of temperature devices found

DeviceAddress tempDeviceAddress; // We'll use this variable to store a found device address



int floorhigh =   10; 
int floorlow = 5; 

bool relay1status;
bool relay2status;
void setup(void)
{
  int error;
  pinMode(RELAY1, OUTPUT);  pinMode(RELAY2, OUTPUT);

  digitalWrite(RELAY1, 0); relay1status = 1;
  digitalWrite(RELAY2, 0); relay2status = 1;
  dht.begin();

  Serial.begin(9600);
  Serial.println("LCD...");

  // wait on Serial to be available on Leonardo
  while (!Serial)
    ;

  Serial.println("Dose: check for LCD");
  Wire.begin();
  Wire.beginTransmission(0x27);
  error = Wire.endTransmission();
  Serial.print("Error: ");
  Serial.print(error);

  if (error == 0) {
    Serial.println(": LCD found.");
    show = 0;
    lcd.begin(16, 2); // initialize the lcd

  } else {
    Serial.println(": LCD not found.");
  } // if
      lcd.setBacklight(255);
          lcd.home();
          lcd.clear();
  
  

  
  // Start up the library
  sensors.begin();
  
  // Grab a count of devices on the wire
  numberOfDevices = sensors.getDeviceCount();
  
  
  Serial.print("Found ");
  Serial.print(numberOfDevices, DEC);
  Serial.println(" devices.");

  // report parasite power requirements
  Serial.print("Parasite power is: "); 
  if (sensors.isParasitePowerMode()) Serial.println("ON");
  else Serial.println("OFF");
  
  // Loop through each device, print out address
  for(int i=0;i<numberOfDevices; i++)
  {
    // Search the wire for address
    if(sensors.getAddress(tempDeviceAddress, i))
  {

    
    
    // set the resolution to TEMPERATURE_PRECISION bit (Each Dallas/Maxim device is capable of several different resolutions)
    sensors.setResolution(tempDeviceAddress, TEMPERATURE_PRECISION);
    
  }else{
    Serial.print("Found ghost device at ");
    Serial.print(i, DEC);
    Serial.print(" but could not detect address. Check power and cabling");
  }
  }


}

// function to print the temperature for a device
void printTemperature(DeviceAddress deviceAddress)
{
  // method 1 - slower
  //Serial.print("Temp C: ");
  //Serial.print(sensors.getTempC(deviceAddress));
  //Serial.print(" Temp F: ");
  //Serial.print(sensors.getTempF(deviceAddress)); // Makes a second call to getTempC and then converts to Fahrenheit

  // method 2 - faster
  float tempC = sensors.getTempC(deviceAddress);
  lcd.print(tempC);
  delay(1000);
//  Serial.print("Temp C: ");
//  Serial.print(tempC);
//  Serial.print(" Temp F: ");
//  Serial.println(DallasTemperature::toFahrenheit(tempC)); // Converts tempC to Fahrenheit
}

void loop(void)
{ 
          lcd.home();
          lcd.clear();
 delay(1000);
  sensors.requestTemperatures(); // Send the com
  dht.read();
   switch(dht.getState()) {
    // всё OK
    case DHT_OK:
    float dhtTC = dht.getTemperatureC();
    float dhtHUM = dht.getHumidity();

      // выводим показания влажности и температуры
      lcd.setCursor(0, 0);
      lcd.print("Temp = ");
      lcd.print(dhtTC);
      lcd.print(" C \t");
      Serial.print("Temperature = ");
      Serial.print(dhtTC);
      Serial.print(" C ");
 //     Serial.print("Temperature = ");
 //     Serial.print(dht.getTemperatureK());
 //     Serial.println(" K \t");
//      Serial.print("Temperature = ");
 //     Serial.print(dht.getTemperatureF());
//      Serial.println(" F \t");
delay(1000);
      lcd.setCursor(0, 1);
      lcd.print("Hum = ");
      lcd.print(dhtHUM);
      lcd.print(" %");
      Serial.print("Humidity = ");
      Serial.print(dhtHUM);
      Serial.println(" %");
      break;
    // ошибка контрольной суммы
    case DHT_ERROR_CHECKSUM:
      Serial.println("Checksum error");
      break;
    // превышение времени ожидания
    case DHT_ERROR_TIMEOUT:
      Serial.println("Time out error");
      break;
    // данных нет, датчик не реагирует или отсутствует
    case DHT_ERROR_NO_REPLY:
      Serial.println("Sensor not connected");
      break;
  }
   delay (4000);
   lcd.clear();
  
  for(int i=0;i<numberOfDevices; i++)
  {
    // Search the wire for address
    if(sensors.getAddress(tempDeviceAddress, i))
  {
   float senstempC = sensors.getTempC(tempDeviceAddress); 
   if (senstempC > floorhigh ) { digitalWrite(RELAY1, 1); relay1status = 0; digitalWrite(RELAY2, 1); relay2status = 0; }
   else if (senstempC < floorlow )  { digitalWrite(RELAY1, 0); relay1status = 1; digitalWrite(RELAY2, 0); relay2status = 1;}
   
   lcd.setCursor(0, 0);
   printAddress(tempDeviceAddress);
   lcd.setCursor(0, 1);
   lcd.print(" ");
   lcd.print(senstempC);
   lcd.print(" ");
   lcd.print(relay1status);
   Serial.print(senstempC);
    Serial.print(" ");
   Serial.print(relay1status);
    Serial.print(" ");
   Serial.print(relay2status);

   delay (4000);
   
  }
  }
  Serial.println();
  
}

// function to print a device address
void printAddress(DeviceAddress deviceAddress)
{
  Serial.print(" ");
  for (uint8_t i = 0; i < 8; i++)
  {
    if (deviceAddress[i] < 16) lcd.print("0");
    lcd.print(deviceAddress[i], HEX);
    Serial.print(deviceAddress[i], HEX);
  }
    Serial.print(" = ");
}
